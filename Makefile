# Variable definitions

PLATFORM	= $(shell uname)
CC		= gcc
LINK		= ${CC}
BIN		= ./bin
OBJDIR		= ./obj
SRC		= ./src
DOC		= ./doc
INCLUDES	= ./include
INCFLAGS	:= -I${INCLUDES}
OPTFLAGS	:= -g
CCFLAGS		:= -c ${OPTFLAGS} -Wall -std=c99

MYLIB		= ${OBJDIR}/mylib.a

# The list of objects to include in the library

MYLIBOBJS	:= ${OBJDIR}/patch.o

${OBJDIR}/%.o : ${SRC}/%.c ${MYLIB} dirs
	${CC} ${CCFLAGS} ${INCFLAGS} $< -o $@

# Main target of the makefile. To build specific targets, call "make <target_name>"

TARGETS		= computePatchOpt doc

all : ${TARGETS}

computePatchOpt : ${OBJDIR}/computePatchOpt.o ${MYLIB} dirs
	${LINK} -o ${BIN}/computePatchOpt ${OBJDIR}/computePatchOpt.o ${MYLIB}

# Building of the library mylib

${MYLIB} : ${MYLIBOBJS}
	ar rcs ${MYLIB} ${MYLIBOBJS}

# For this project

dirs:
	mkdir -p $(BIN)
	mkdir -p $(OBJDIR)

doc:
	doxygen Doxyfile

clean:
	rm -rf $(OBJDIR)
	rm -rf $(BIN)
	rm -rf $(DOC)

.PHONY: clean all dirs doc
