/**
 * \file patch.h
 * \authors {Julien BITAILLOU, Thomas FOULON}
 */

#ifndef PATCH_H
#define PATCH_H

#include <inttypes.h>

typedef enum BOOL{
    FALSE,
    TRUE
} BOOL;

typedef enum instruction_type{
    ADDITION,
    SUBSTITUTION,
    DESTRUCTION
} instruction_type;

typedef struct instruction{
    instruction_type type;
    uint32_t affected_line;
    char *line_A;
    char *line_B;
    struct instruction *previous;
} instruction;

instruction *new_instruction(instruction_type type, uint32_t affected_line, char *line_A, char *line_B);

void delete_instruction(instruction *i);

typedef struct patch{
    uint32_t nb_lines_A;
    uint32_t nb_lines_B;
    uint32_t cost;
    instruction *last;
} patch;

patch *new_patch(uint32_t nb_lines_A, uint32_t nb_lines_B);

void delete_patch(patch *p, BOOL delete_instructions);

void add_instruction(patch *p, instruction *i, uint32_t cost);

char *to_string(patch *p);

#endif /* PATCH_H */

