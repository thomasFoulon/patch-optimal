from PyQt5 import QtCore, QtGui, QtWidgets
from gui.collapsible_widget import CollapsibleWidget
from src.tp import *
from src.project import Project
from gui.killable_thread_button import KillableThreadButton

class QuestionWidget(CollapsibleWidget):
    """
    Widget for question.
    Contains a "run" KillableThreadButton.

    Attributes
    ----------
    tp: TP
        The current TP
    question: Question
        Reference to the question object
    """

    def __init__(self, tp: TP, question: Question, parent = None):
        """
        QuestionWidget constructor

        Arguments
        ---------
        tp: TP
            The current TP
        question: Question
            Reference to the question object
        project: Project
            Reference to the project (to get the exe file)
        parent
            Optional parent widget
        """
        super(QuestionWidget, self).__init__(
            question.name,
            parent = parent,
            show_icon = False,
            text_read_only = False,
            grade_read_only = False)
        self.tp = tp
        self.question = question
        self.setContentLayout()
        self.setBarem(question.barem)

    def getComment(self) -> str:
        return self.commentText.toPlainText()

    def getGrade(self) -> float:
        return self.gradeWidget.getGrade()
