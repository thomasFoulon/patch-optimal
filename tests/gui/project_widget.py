from PyQt5 import QtWidgets, QtCore
from gui.benchlauncher_widget import BenchLauncherWidget
from gui.report_widget import ReportWidget
from src.tp import *
from src.project import *

class ProjectWidget(QtWidgets.QSplitter):

    def __init__(self, parent, tp : TP, show_report = True):
        super(ProjectWidget, self).__init__(QtCore.Qt.Horizontal, parent)

        self.project = Project()
        self.tp = tp
        self.show_report = show_report

        self.benchlauncher = BenchLauncherWidget(self, self.tp, self.project)
        self.addWidget(self.benchlauncher)

        if self.show_report:
            self.report = ReportWidget(self, self.tp, self.project)
            self.addWidget(self.report)

        self.setChildrenCollapsible(True)
        self.setHandleWidth(1)
        self.setOpaqueResize(False)

    def loadDB(self, db_conn, id):
        self.benchlauncher.loadDB(db_conn, id)
        if self.show_report:
            self.report.loadDB(db_conn, id)

    def saveDB(self, db_conn, id):
        self.benchlauncher.saveDB(db_conn, id)
        if self.show_report:
            self.report.saveDB(db_conn, id)
