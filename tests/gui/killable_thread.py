import threading
import ctypes
from src.utils import *

class KillableThread(threading.Thread):

    def __init__(self, cmd, callback):
        super(KillableThread, self).__init__(
            target = self.target
            args = (cmd, callback)
            )

    def target(self, cmd, callback):
        res = silentRunner(cmd)
        callback(res)

    def get_id(self):
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def kill(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
