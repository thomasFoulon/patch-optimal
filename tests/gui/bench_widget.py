from PyQt5 import QtCore, QtGui, QtWidgets
from gui.collapsible_widget import CollapsibleWidget
from src.tp import TP, Benchmark
from src.project import Project
from gui.killable_thread_button import KillableThreadButton

class BenchWidget(CollapsibleWidget):
    """
    Widget for running a benchmark.
    Contains a "run" KillableThreadButton.

    Attributes
    ----------
    tp: TP
        The current TP
    bench: Benchmatk
        Reference to the benchmark object
    project: Project
        Reference to the project (to get the exe file)
    """

    def __init__(self, tp: TP, bench: Benchmark, project: Project, parent = None):
        """
        BenchWidget constructor

        Arguments
        ---------
        tp: TP
            The current TP
        bench: Benchmatk
            Reference to the benchmark object
        project: Project
            Reference to the project (to get the exe file)
        parent
            Optional parent widget
        """
        super(BenchWidget, self).__init__(bench.name, parent)
        self.tp = tp
        self.bench = bench
        self.project = project
        self.setContent()
        self.setBarem(bench.barem)

    def setContent(self):
        """
        Set up the widget content
        """
        self.runButton = KillableThreadButton(
            cmd = lambda: self.tp.cmd(self.project, self.bench),
            callback = self.callback,
            label="run",
            box = self
        )
        self.gridLayout.addWidget(self.runButton, 0, 2)
        self.setContentLayout()

    def callback(self, res) -> None:
        """
        Function called when the benchmark is done
        """
        self.result = self.tp.checkResult(self.project, self.bench, res)
        self.resultSignal.emit(self.result)

    def setDisabled(self, b: bool) -> None:
        """
        Enable or disable the "run" button

        Arguments
        ---------
        b: bool
            If True, disable the "run" button
        """
        self.runButton.setDisabled(b)
