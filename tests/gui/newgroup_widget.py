from PyQt5 import QtWidgets
from src.tp import TP
from gui.bordered_widget import BorderedWidget

class NewGroupWidget(QtWidgets.QDialog):

    def __init__(self, parent):
        super().__init__(parent)

        self.setWindowTitle("Adding new group")

        # Block inputs in other windows
        self.setModal(True)

        # Widget dimensions
        self.height = 300
        self.width = 400
        self.setFixedHeight(self.height)
        self.setFixedWidth(self.width)

        self.setAutoFillBackground(True)
        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.textFields = []
        self.addTextField()
        self.addTextField()

        button = QtWidgets.QPushButton("OK")
        button.pressed.connect(self.ok_pressed)
        self.layout.addWidget(button)

        button = QtWidgets.QPushButton("Cancel")
        button.pressed.connect(self.reject)
        self.layout.addWidget(button)

        center = parent.rect().center()
        self.move(
            center.x() - self.width/2,
            center.y() - self.height/2
        )

        self.show()

    def addTextField(self):
        textField = QtWidgets.QLineEdit()
        textField.setReadOnly(False)
        self.textFields.append(textField)
        self.layout.addWidget(
            BorderedWidget(
                textField,
                "Student: Firstname Lastname"
            )
        )

    def ok_pressed(self):
        group_name = "\n".join([
            field.text() for field in self.textFields
            if len(field.text()) > 0
        ])
        self.parent().newGroupButton(group_name)
        self.accept()
