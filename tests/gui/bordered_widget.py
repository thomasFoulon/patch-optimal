from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFrame

class BorderedWidget(QFrame):

    def __init__(self, widget, name = ""):
        super(BorderedWidget, self).__init__()
        #self.setStyleSheet("background-color: rgb(255,0,0); margin:5px; border:1px solid rgb(0, 255, 0); ")
        self.setFrameStyle(QFrame.Panel | QFrame.Raised);

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel(name))
        layout.addWidget(widget)
        self.setLayout(layout)
