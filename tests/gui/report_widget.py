from PyQt5 import QtWidgets, QtCore
import os
from src.tp import *
from src.project import *
from gui.question_widget import QuestionWidget

class ReportWidget(QtWidgets.QWidget):

    def __init__(self, parent, tp: TP, project: Project):
        super(ReportWidget, self).__init__(parent)

        self.tp = tp
        self.project = project
        self.initUI()

    def initUI(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(QtCore.Qt.AlignTop)
        self.setLayout(self.layout)

        # Open report button
        button = QtWidgets.QPushButton('Open report')
        self.layout.addWidget(button)
        button.pressed.connect(self.open_report_pressed)

        # Open doc button
        button = QtWidgets.QPushButton('Open doc')
        self.layout.addWidget(button)
        button.pressed.connect(self.open_doc_pressed)

        # scroll area widget contents
        self.scrollLayout = QtWidgets.QVBoxLayout()
        self.scrollLayout.setAlignment(QtCore.Qt.AlignTop)
        self.scrollWidget = QtWidgets.QWidget()
        self.scrollWidget.setLayout(self.scrollLayout)
        # scroll area
        scrollArea = QtWidgets.QScrollArea()
        scrollArea.setWidget(self.scrollWidget)
        scrollArea.setWidgetResizable(True)
        self.layout.addWidget(scrollArea)

        self.questionBoxes = []
        self.setQuestions()

    def setQuestions(self) -> None:
        """
        Set up the question widgets
        """
        for question in self.tp.questions:
            box = QuestionWidget(self.tp, question, parent = self)
            self.questionBoxes.append(box)
            self.scrollLayout.addWidget(box)

    def getReportPath(self) -> str:
        folder = self.project.getFolder()
        if folder != None:
            return os.path.join(folder, 'rapport', 'rapport.pdf')
        return None

    def open_report_pressed(self):
        file = self.getReportPath()
        if file is None:
            raise Exception('The project folder was not specified')
        elif os.path.isfile(file):
            cmd = 'evince %s &' % file
            os.system(cmd)
        else:
            raise Exception('File %s does not exist' % file)

    def getDocPath(self) -> str:
        folder = self.project.getFolder()
        if folder != None:
            path = os.path.join(folder, 'doc', 'index.html')
            if os.path.isfile(path):
                return path
            else:
                return os.path.join(folder, 'doc', 'html', 'index.html')
        return None

    def open_doc_pressed(self):
        file = self.getDocPath()
        if file is None:
            raise Exception('The project folder was not specified')
        elif os.path.isfile(file):
            cmd = 'firefox %s &' % file
            os.system(cmd)
        else:
            raise Exception('File %s does not exist' % file)

    def saveDB(self, conn, id) -> None:
        cmd = '''INSERT OR REPLACE INTO %s (
            id,
            question_name,
            grade,
            comment
        ) VALUES (?,?,?,?)''' % self.tp.getReportTableName()
        c = conn.cursor()
        for questionBox in self.questionBoxes:
            c.execute(cmd, (
                id,
                questionBox.question.name,
                questionBox.getGrade(),
                questionBox.getComment(),
                )
        )

    def loadDB(self, conn, id) -> None:
        c = conn.cursor()
        for questionBox in self.questionBoxes:
            cmd = '''SELECT comment, grade FROM %s
            WHERE id = ?
            AND question_name = ?
            ''' % self.tp.getReportTableName()
            c.execute(cmd, (id, questionBox.question.name, ))
            res = c.fetchone()
            if res != None:
                questionBox.loadResult(Result(
                    comment = res[0],
                    grade = res[1]
                ))
