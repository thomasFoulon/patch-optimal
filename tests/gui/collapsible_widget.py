from PyQt5 import QtCore, QtGui, QtWidgets
from src.tp import *
from gui.loading_widget import QtWaitingSpinner
from gui.bordered_widget import BorderedWidget
from gui.grade_widget import GradeWidget

class CollapsibleWidget(QtWidgets.QWidget):
    """
    Widget that can be collapsed.
    It is used to handle benchmarks, compilation and questions.
    It features:
        * a status icon
        * a waiting spinner that toggles when the status is 'RUNNING'
        * a comment text area
        * a grade area
    """

    # Signal used to update the widget w.r.t a result
    resultSignal = QtCore.pyqtSignal(Result)
    # Signal used to update the text of the comment
    textSignal = QtCore.pyqtSignal(str)
    # Signal to provide the execution time of the task
    #timeSignal = QtCore.pyqtSignal(float)

    def __init__(self,
        title: str = "",
        parent=None,
        show_icon = True,
        text_read_only = True,
        grade_read_only = True):
        """
        Widget constructor

        Arguments
        ---------
        title: str
            Name of the widget, that appears next to the collapsing button
        parent
            Optional parent widget
        show_icon: bool
            True if the status icon must be displayed
        text_read_only: bool
            True if the comment text area must be read only
        grade_read_only: bool
            True if the grade area is read only
        """
        super(CollapsibleWidget, self).__init__(parent)

        self.show_icon = show_icon
        self.text_read_only = text_read_only
        self.grade_read_only = grade_read_only
        self.setGridLayout()
        self.setContentArea()
        self.setToggleButton(title)
        self.setAnimations()
        self.setWaitingWidget()
        self.setIcon()
        self.setGradeWidget()

        self.resultSignal.connect(self.loadResult)
        self.textSignal.connect(lambda str: self.commentText.append(str))
        #self.timeSignal.connect(self.setTime)

        self.loadResult(Result())

    def setGridLayout(self) -> None:
        """
        Set up the grid layout of the widget
        """
        self.gridLayout = QtWidgets.QGridLayout(self)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setColumnMinimumWidth(3, 30)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.gridLayout)

    def setContentArea(self) -> None:
        """
        Set up the collapsible content area of the widget.
        This content has a QVBoxLayout.
        """
        self.content_area = QtWidgets.QScrollArea(
            maximumHeight=0, minimumHeight=0
        )
        self.content_area.setSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed
        )
        self.content_area.setFrameShape(QtWidgets.QFrame.NoFrame)

        self.gridLayout.addWidget(self.content_area, 1, 0)

        self.contentLayout = QtWidgets.QVBoxLayout()
        self.content_area.setLayout(self.contentLayout)

    def setToggleButton(self, title) -> None:
        """
        Set up the toggle button (to collapse the content)
        """
        self.toggle_button = QtWidgets.QToolButton(
            text=title, checkable=True, checked=False
        )

        self.toggle_button.setStyleSheet("QToolButton { border: none; }")
        self.toggle_button.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextBesideIcon
        )
        self.toggle_button.setArrowType(QtCore.Qt.RightArrow)
        self.toggle_button.pressed.connect(self.on_pressed)
        self.gridLayout.addWidget(self.toggle_button, 0 ,0)

    def setAnimations(self) -> None:
        """
        Set up the animation related to collapsing
        """
        self.toggle_animation = QtCore.QParallelAnimationGroup(self)
        self.toggle_animation.addAnimation(
            QtCore.QPropertyAnimation(self, b"minimumHeight")
        )
        self.toggle_animation.addAnimation(
            QtCore.QPropertyAnimation(self, b"maximumHeight")
        )
        self.toggle_animation.addAnimation(
            QtCore.QPropertyAnimation(self.content_area, b"maximumHeight")
        )

    def setWaitingWidget(self) -> None:
        """
        Set up the waiting spinner
        """
        self.waitingWidget = QtWaitingSpinner(
            centerOnParent = False,
            disableParentWhenSpinning = False
        )
        self.gridLayout.addWidget(self.waitingWidget, 0, 3)

    def setIcon(self) -> None:
        """
        Set up the status icon
        """
        self.pellet = QtWidgets.QToolButton(
            text="", checkable=True, checked=False
        )
        self.pellet.setIcon(QtWidgets.QApplication.style().standardIcon(
             QtWidgets.QStyle.SP_DialogCloseButton
        ))
        self.pellet.setStyleSheet("QToolButton { border: none; }")
        self.gridLayout.addWidget(self.pellet, 0, 4)
        if self.show_icon:
            self.pellet.show()
        else:
            self.pellet.hide()

    def open(self) -> None:
        """
        Action associated with the collapsing
        """
        if not self.toggle_button.isChecked():
            self.toggle_button.setArrowType(
                QtCore.Qt.DownArrow
            )
            self.toggle_animation.setDirection(
                QtCore.QAbstractAnimation.Forward
            )
            self.toggle_animation.start()

    def close(self) -> None:
        """
        Action of decollapsing
        """
        if self.toggle_button.isChecked():
            self.toggle_button.setArrowType(
                QtCore.Qt.RightArrow
            )
            self.toggle_animation.setDirection(
                QtCore.QAbstractAnimation.Backward
            )
            self.toggle_animation.start()

    @QtCore.pyqtSlot()
    def on_pressed(self):
        if not self.toggle_button.isChecked():
            self.open()
        else:
            self.close()

    def setCommentArea(self) -> None:
        """
        Set up the comment area within the content area
        """
        self.commentText = QtWidgets.QTextEdit("")
        self.commentText.setMaximumHeight(120)
        self.commentText.setReadOnly(self.text_read_only)
        self.contentLayout.addWidget(
            BorderedWidget(
                self.commentText,
                "Comments"
            )
        )

    # def setTimeField(self) -> None:
    #     """
    #     Set up the time field within the content area
    #     """
    #     self.timeField = QtWidgets.QLineEdit("")
    #     self.commentText.setReadOnly(True)
    #     self.contentLayout.addWidget(
    #         BorderedWidget(
    #             self.timeField,
    #             "Execution time"
    #         )
    #     )

    def setGradeWidget(self) -> None:
        """
        Set up the grade area
        """
        self.gradeWidget = GradeWidget(
            parent = self,
            read_only = self.grade_read_only
        )
        self.gridLayout.addWidget(self.gradeWidget, 0, 5)

    def setContentLayout(self) -> None:
        """
        Set up the content layout.
        This must be called after all widgets have been added within the content area.
        """
        self.setCommentArea()
        #self.setTimeField()

        collapsed_height = (
            self.sizeHint().height() - self.content_area.maximumHeight()
        )
        content_height = self.contentLayout.sizeHint().height()
        for i in range(self.toggle_animation.animationCount()):
            animation = self.toggle_animation.animationAt(i)
            animation.setDuration(500)
            animation.setStartValue(collapsed_height)
            animation.setEndValue(collapsed_height + content_height)

        content_animation = self.toggle_animation.animationAt(
            self.toggle_animation.animationCount() - 1
        )
        content_animation.setDuration(500)
        content_animation.setStartValue(0)
        content_animation.setEndValue(content_height)

    def setStatus(self, status: Status) -> None:
        """
        Applies a status on the widget.
        It updates the status icon and the waiting spinner.
        """
        if status is Status.RUNNING:
            self.waitingWidget.start()
            icon = QtWidgets.QStyle.SP_TitleBarContextHelpButton
        else:
            self.waitingWidget.stop()
            if status is Status.OK:
                icon = QtWidgets.QStyle.SP_DialogYesButton
            elif status is Status.ERROR:
                icon = QtWidgets.QStyle.SP_DialogNoButton
            elif status is Status.WAITING:
                icon = QtWidgets.QStyle.SP_TitleBarContextHelpButton
            elif status is Status.BOF:
                icon = QtWidgets.QStyle.SP_MessageBoxWarning
        self.pellet.setIcon(QtWidgets.QApplication.style().standardIcon(icon))

    def setCommentText(self, text: str) -> None:
        """
        Updates the text of the comment area
        """
        if hasattr(self, 'commentText'):
            self.commentText.append(text)

    def setGrade(self, grade: float) -> None:
        """
        Updates the grade
        """
        self.gradeWidget.setGrade(grade)

    def setBarem(self, barem: float) -> None:
        """
        Updates de barem
        """
        self.gradeWidget.setBarem(barem)

    def loadResult(self, result: Result) -> None:
        """
        Loads a result.
        Updates the comment and status icon accordingly
        """
        self.result = result
        self.setCommentText(result.getComment())
        self.setStatus(result.getStatus())
        self.setGrade(result.getGrade())

    # def setTime(self, time: float) -> None:
    #     self.timeField.setText(str(time))

    def clearText(self) -> None:
        self.commentText.setText('')
