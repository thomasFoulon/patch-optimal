from PyQt5 import QtWidgets

class GradeWidget(QtWidgets.QWidget):

    def __init__(self, parent = None, read_only = False):
        super(GradeWidget, self).__init__(parent)

        self.layout = QtWidgets.QHBoxLayout()
        self.setLayout(self.layout)

        self.line = QtWidgets.QLineEdit("")
        self.line.setMaximumWidth(40)
        self.line.setMaximumHeight(35)
        self.line.setReadOnly(read_only)
        self.layout.addWidget(self.line)

        self.barem = QtWidgets.QLabel('/0.0')
        self.barem.setMaximumWidth(40)
        self.barem.setMaximumHeight(35)
        self.layout.addWidget(self.barem)

        self.setMaximumWidth(80)
        self.setMaximumHeight(self.line.height()+5)

    def setGrade(self, grade: float) -> None:
        if grade != None:
            self.line.setText(str(grade))

    def setBarem(self, barem: float) -> None:
        if barem != None:
            self.barem.setText('/' + str(barem))

    def getGrade(self) -> float:
        if self.line.text() != "":
            return float(self.line.text())
        return None
