from PyQt5 import QtWidgets, QtCore
from gui.project_widget import ProjectWidget

class GroupWidget(QtWidgets.QTabWidget):

    def __init__(self, parent, tps, show_report = True):
        super(GroupWidget, self).__init__(parent)
        self.projects = []
        for tp in tps:
            self.projects.append(ProjectWidget(self, tp, show_report))
            self.addTab(self.projects[-1], tp.name)

    def loadDB(self, db_conn, id):
        for project in self.projects:
            project.loadDB(db_conn, id)

    def saveDB(self, db_conn, id):
        for project in self.projects:
            project.saveDB(db_conn, id)

class GroupButton(QtWidgets.QPushButton):
    """
    A button for selecting a group in the left scroll panel.

    Attributes
    ----------
    mainWidget: QWidget
        The widget on which the project is loaded
    id: str
        The identifiant of the group in the DB
    """

    def __init__(self, mainWidget, id: str, tps):
        super(GroupButton, self).__init__(
            GroupButton.makeLabel(id)
        )
        self.mainWidget = mainWidget
        self.id = id
        self.setStyleSheet("QPushButton { text-align: left; }")
        self.groupWidget = GroupWidget(mainWidget, tps)
        self.pressed.connect(self.on_pressed)
        self.setCheckable(True)

        self.setContextMenu()

    def setContextMenu(self):
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)
        self.popMenu = QtWidgets.QMenu(self)
        self.popMenu.addAction(QtWidgets.QAction('Delete', self))

    def on_pressed(self):
        """
        Clears all widgets from mainLayout
        """
        layout = self.mainWidget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        layout.addWidget(self.groupWidget)

    def on_context_menu(self, point):
        # show context menu
        self.popMenu.exec_(self.mapToGlobal(point))

    def loadDB(self, db_conn):
        self.groupWidget.loadDB(db_conn, self.id)

    def saveGroup(self, db_conn):
        cmd = '''INSERT OR REPLACE INTO groups (names) VALUES (?)'''
        c = db_conn.cursor()
        c.execute(cmd, (self.id,))

    def saveDB(self, db_conn):
        self.saveGroup(db_conn)
        self.groupWidget.saveDB(db_conn, self.id)

    def makeLabel(s: str) -> str:
        return s.replace(';','\n')
