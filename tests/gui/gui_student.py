from PyQt5 import QtCore, QtGui, QtWidgets
from gui.groupbutton_widget import GroupWidget
from tps.diff import Diff

class Main(QtWidgets.QMainWindow):

    tps = [
        Diff()
    ]

    def __init__(self):
        super().__init__()
        self.initUI()
        # Window full screen
        self.showMaximized()

    def initUI(self):
        self.setGeometry(300, 300, 600, 600)
        self.setWindowTitle('AOD')
        #icon = QIcon(get_ressources_file('icon.png'))
        #self.setWindowIcon(icon)

        #self.set_menu()
        self.setMainWidget()
        self.show()

    def setMainWidget(self):

        # main layout
        self.mainLayout = QtWidgets.QHBoxLayout()
        self.mainLayout.addWidget(GroupWidget(self, self.tps, show_report = False))

        centralWidget = QtWidgets.QWidget()
        centralWidget.setLayout(self.mainLayout)
        self.setCentralWidget(centralWidget)
