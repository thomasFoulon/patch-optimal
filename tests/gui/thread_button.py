from PyQt5 import QtWidgets
import threading
from src.tp import Status
import time

class ThreadButton(QtWidgets.QPushButton):
    """
    Class of button associated with a function.
    When pressed, a thread is created to execute the function.

    Attributes
    ----------
    f: function
        The function to execute when pressed
    label: str
        Button label
    running: bool
        True if the thread is running
    box: CollapsibleBox
        An optional box whose status can be updated
    """

    def __init__(self, f, label = "", box = None):
        """
        ThreadButton constructor

        Arguments
        ---------
        f: function
            The function to execute when pressed
        label:
            Button label
        box: CollapsibleBox
            An optional box whose status can be updated
        """
        super(ThreadButton, self).__init__(label)
        self.f = f
        self.label = label
        self.running = False
        self.pressed.connect(self.onPressed)
        self.box = box

    def thread_f(self):
        """
        Function executed by the thread.
        """
        self.f()
        self.stop()
        #print(time.clock_gettime(time.CLOCK_THREAD_CPUTIME_ID))
        # if self.box:
        #     exe_time = time.clock_gettime(time.CLOCK_THREAD_CPUTIME_ID)
        #     self.box.timeSignal.emit(exe_time)

    def stop(self):
        self.setDisabled(False)
        self.running = False

    def start(self):
        self.setDisabled(True)
        self.running = True
        if self.box:
            self.box.setStatus(Status.RUNNING)
        self.thread = threading.Thread(
            target = self.thread_f,
            args = ()
        )
        self.thread.start()

    def onPressed(self):
        if self.running:
            self.thread._stop()
            self.stop()
        else:
            self.start()
