from PyQt5 import QtWidgets, QtCore
from gui.collapsible_widget import CollapsibleWidget
from gui.bench_widget import BenchWidget
from src.tp import *
from src.project import *
from gui.thread_button import ThreadButton
from gui.bordered_widget import BorderedWidget

class BenchLauncherWidget(QtWidgets.QWidget):
    """
    Widget that handles benchmarks of a project

    Attributes
    ----------
    tp: TP
        The current TP
    project: Project
        Handling of project files
    compileBox: CollapsibleWidget
        Widget handling the project compilation
    benchmarkBoxes: BenchWidget list
        List of benchmark widgets
        They are enabled whenever the project compiles
    """

    def __init__(self, parent, tp : TP, project: Project):
        """
        ProjectWidget constructor

        Arguments
        ---------
        parent
            The parent widget
        tp: TP
        """
        super(BenchLauncherWidget, self).__init__(parent)

        self.tp = tp
        self.project = project
        self.benchmarkBoxes = []

        # scroll area widget contents - layout
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(QtCore.Qt.AlignTop)
        self.setLayout(self.layout)

        self.setCompilation()
        self.setRunBenchmarksButton()

        # scroll area widget contents
        self.scrollLayout = QtWidgets.QVBoxLayout()
        self.scrollLayout.setAlignment(QtCore.Qt.AlignTop)
        self.scrollWidget = QtWidgets.QWidget()
        self.scrollWidget.setLayout(self.scrollLayout)

        # scroll area
        scrollArea = QtWidgets.QScrollArea()
        scrollArea.setWidget(self.scrollWidget)
        scrollArea.setWidgetResizable(True)

        self.layout.addWidget(scrollArea)
        self.setFixedWidth(800)
        self.setBenchmarks()

    def setCompilation(self):
        """
        Set up the compilation widget
        """
        self.compileBox = CollapsibleWidget("Compilation")
        self.layout.addWidget(self.compileBox)

        self.compileBox.text = QtWidgets.QLineEdit()
        self.compileBox.text.setReadOnly(True)
        self.compileBox.contentLayout.addWidget(
            BorderedWidget(
                self.compileBox.text,
                "Path"
            )
        )
        self.compileBox.setBarem(self.tp.compil_barem)

        # Set path button
        set_button = QtWidgets.QPushButton(
            text = "Set path"
        )
        set_button.pressed.connect(self.setPath_pressed)
        self.compileBox.gridLayout.addWidget(set_button, 0, 1)
        self.setCompileButton()
        self.compileBox.setContentLayout()
        #self.layout.addStretch()

    def setRunBenchmarksButton(self) -> None:
        """
        Set up a button for running all benchmarks concurrently
        """
        self.runAllButton = QtWidgets.QPushButton("Run All Benchmarks")
        def f():
            for benchmarkBox in self.benchmarkBoxes:
                benchmarkBox.runButton.pressed.emit()
        self.runAllButton.pressed.connect(f)
        self.runAllButton.setDisabled(True)
        self.layout.addWidget(self.runAllButton)

    def setCompileButton(self) -> None:
        """
        Set up a button for compiling the project
        """
        self.thread_button = ThreadButton(
            self.compile_pressed,
            label = "compile",
            box = self.compileBox
        )
        self.compileBox.gridLayout.addWidget(self.thread_button, 0, 2)

    def setBenchmarks(self) -> None:
        """
        Set up the benchmark widgets
        """
        for bench in self.tp.benchmarks:
            box = BenchWidget(self.tp, bench, self.project)
            self.benchmarkBoxes.append(box)
            self.scrollLayout.addWidget(box)
            box.setDisabled(True)

    def setPath_pressed(self):
        """
        Function called when the setPath button is pressed.
        It launches the compilation.
        """
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        dialog = QtWidgets.QFileDialog()
        dialog.setOptions(options)
        dialog.setFileMode(QtWidgets.QFileDialog.DirectoryOnly)

        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            folder = dialog.selectedFiles()[0]
            self.setFolder(folder)
            self.thread_button.pressed.emit()

    def setFolder(self, folder: str) -> None:
        self.compileBox.text.setText(folder)
        self.project.setFolder(folder)
        self.project.setExe(self.tp.exe)

    def compile_pressed(self) -> None:
        """
        Function called when the compile button is pressed
        """
        result = Result()
        try:
            self.project.load()
            result.comment = "Compilation successful"
            result.status = Status.OK
            result.grade = self.tp.compil_barem
            self.disableBenchmarks(False)
        except ProjectException as e:
            result.comment = str(e)
            result.status = Status.ERROR
            result.grade = 0.0
            self.disableBenchmarks(True)
        self.compileBox.resultSignal.emit(result)

    def disableBenchmarks(self, b: bool) -> None:
        """
        Enable or disable the benchmark widgets

        Arguments
        ---------
        b: bool
            If True, disable the benchmark widgets
        """
        self.runAllButton.setDisabled(b)
        for benchBox in self.benchmarkBoxes:
            benchBox.setDisabled(b)

    def loadDB_compil(self, conn, id) -> None:
        c = conn.cursor()
        cmd = '''SELECT path, status, comment FROM %s
        WHERE id = ?
        ''' % self.tp.getCompilTableName()
        c.execute(cmd, (id, ))
        res = c.fetchone()
        if res != None and res[0] != None:
            self.setFolder(res[0])
            self.compileBox.loadResult(Result(
                status = Status.ofString(res[1]),
                comment = res[2],
            ))
            if self.compileBox.result.status == Status.OK:
                self.disableBenchmarks(False)

    def loadDB_benchs(self, conn, id) -> None:
        c = conn.cursor()
        for benchBox in self.benchmarkBoxes:
            cmd = '''SELECT status, comment, grade FROM %s
            WHERE id = ?
            AND bench_name = ?
            ''' % self.tp.getBenchTableName()
            c.execute(cmd, (id, benchBox.bench.name, ))
            res = c.fetchone()
            if res != None:
                benchBox.loadResult(Result(
                    status = Status.ofString(res[0]),
                    comment = res[1],
                    grade = res[2]
                ))

    def loadDB(self, conn, id) -> None:
        self.loadDB_compil(conn, id)
        self.loadDB_benchs(conn, id)

    def saveDB_compil(self, conn, id) -> None:
        cmd = '''INSERT OR REPLACE INTO %s (
            id,
            path,
            status,
            comment
        ) VALUES (?,?,?,?)''' % self.tp.getCompilTableName()
        c = conn.cursor()
        c.execute(cmd, (
            id,
            self.project.getFolder(),
            Status.toString(self.compileBox.result.status),
            self.compileBox.result.comment,
            )
        )

    def saveDB_benchs(self, conn, id) -> None:
        cmd = '''INSERT OR REPLACE INTO %s (
            id,
            bench_name,
            status,
            grade,
            comment
        ) VALUES (?,?,?,?,?)''' % self.tp.getBenchTableName()
        c = conn.cursor()
        for benchBox in self.benchmarkBoxes:
            c.execute(cmd, (
                id,
                benchBox.bench.name,
                Status.toString(benchBox.result.status),
                benchBox.result.grade,
                benchBox.result.comment,
                )
        )

    def saveDB(self, conn, id) -> None:
        self.saveDB_compil(conn, id)
        self.saveDB_benchs(conn, id)
