from PyQt5 import QtWidgets
import subprocess, os, time, threading
from src.tp import *
from src.utils import *

class KillableThreadButton(QtWidgets.QPushButton):
    """
    Class of button that launches a command, followed by a callback function.
    When pressed, a thread is created to execute the command.
    Contraty to the ThreadButton class, the command can be interrupted.
    The callback is called even if the command is interrupted.

    Attributes
    ----------
    cmd: string
        The command to execute when pressed
    callback: function
        A function to call after cmd terminates
    label: str
        Button label
    running: bool
        True if the thread is running
    kill: bool
        Set to True to interrupt the command
    box: CollapsibleBox
        An optional box whose status can be updated
    """

    def __init__(self, cmd, callback, label = "", box = None):
        """
        ThreadButton constructor

        Arguments
        ---------
        cmd: string
            The command to execute when pressed
        callback: function
            A function to call after cmd terminates
        label:
            Button label
        box: CollapsibleBox
            An optional box whose status can be updated
        """
        super(KillableThreadButton, self).__init__(label)
        self.cmd = cmd
        self.callback = callback
        self.label = label
        self.running = False
        self.pressed.connect(self.onPressed)
        self.box = box
        self.kill = False

    def thread_f(self):
        """
        Function executed by the thread.
        """
        (out_file, in_file, file_name) = makeLogFile()
        p = runner(self.cmd, out = out_file, err = out_file)
        while True:
            time.sleep(1.0)
            t = in_file.read().decode('utf8')
            if len(t) > 0:
                self.box.textSignal.emit(t)
            if self.kill:
                p.terminate()
                rc = -1
                break
            rc = p.poll()
            if rc is not None:
                break
        try:
            out_file.close()
            in_file.close()
        except:
            pass
        os.remove(file_name)
        self.kill = False
        self.callback(rc)
        self.stop()

    # def thread_f(self):
    #     """
    #     Function executed by the thread.
    #     """
    #     p = runner(self.cmd, err = subprocess.STDOUT)
    #     while True:
    #         time.sleep(1.0)
    #         print('waiting')
    #         for line in p.stdout:
    #             print('here')
    #             t = line.decode('ascii')
    #             if len(t) > 0:
    #                 self.box.textSignal.emit(t)
    #         if self.kill:
    #             p.terminate()
    #             rc = -1
    #             break
    #         rc = p.poll()
    #         if rc is not None:
    #             break
    #     self.kill = False
    #     self.callback(rc)
        # self.stop()

    def stop(self):
        self.setText(self.label)
        self.running = False

    def start(self):
        self.setText("abort")
        self.running = True
        if self.box:
            self.box.setStatus(Status.RUNNING)
            self.box.clearText()
        self.thread = threading.Thread(
            target = self.thread_f,
            args = ()
        )
        self.thread.start()

    def onPressed(self):
        if self.running:
            self.kill = True
        else:
            self.start()
