import sys
import random
from PyQt5 import QtCore, QtGui, QtWidgets
from tps.diff import Diff
from gui.groupbutton_widget import GroupButton
from gui.newgroup_widget import NewGroupWidget
from src.utils import *

class Main(QtWidgets.QMainWindow):

    tps = [
        Diff()
    ]

    def __init__(self):
        super().__init__()
        self.initUI()
        # Window full screen
        self.showMaximized()

        # Redirects exception
        sys.excepthook = self.raiseWarning

    def raiseWarning(self, type, value, traceback):
        error_dialog = QtWidgets.QMessageBox()
        fname = os.path.split(traceback.tb_frame.f_code.co_filename)[1]
        s = 'file: %s, line: %i\n%s: %s' % (
            fname,
            traceback.tb_lineno,
            type,
            value
        )
        error_dialog.warning(self, 'Error', s)

    def initUI(self):
        self.setGeometry(300, 300, 600, 600)
        self.setWindowTitle('AOD')
        #icon = QIcon(get_ressources_file('icon.png'))
        #self.setWindowIcon(icon)

        self.setMainWidget()
        self.setGroupScroller()
        self.setContentWidget()
        self.setMenu()
        self.show()

        # Initial size of the widgets
        self.mainLayout.setSizes([150,800])

    def setGroupScroller(self):
        # scroll area widget contents
        self.scrollLayout = QtWidgets.QVBoxLayout()
        self.scrollLayout.setAlignment(QtCore.Qt.AlignTop)
        self.scrollWidget = QtWidgets.QWidget()
        self.scrollWidget.setLayout(self.scrollLayout)

        # scroll area
        scrollArea = QtWidgets.QScrollArea()
        scrollArea.setWidget(self.scrollWidget)
        scrollArea.setWidgetResizable(True)
        self.mainLayout.addWidget(scrollArea)

        #button group
        self.buttonGroup = QtWidgets.QButtonGroup()
        self.buttonGroup.setExclusive(True)

        # Add group button
        button = QtWidgets.QPushButton("+")
        self.scrollLayout.addWidget(button)
        button.pressed.connect(self.newGroup_pressed)

    def newGroupButton(self, group_name):
        groupButton = GroupButton(
            self.contentWidget,
            group_name,
            self.tps
        )
        self.scrollLayout.addWidget(groupButton)
        self.buttonGroup.addButton(groupButton)
        return groupButton

    def newGroup_pressed(self):
        dialog = NewGroupWidget(self)
        if (dialog.exec_() == QtWidgets.QDialog.Accepted):
            print('New group added')
        else:
            print('New group cancelled')
        dialog.deleteLater()

    def setContentWidget(self):
        self.contentWidget = QtWidgets.QWidget()
        self.contentWidget.setLayout(QtWidgets.QVBoxLayout())
        self.mainLayout.addWidget(self.contentWidget)

    def setMainWidget(self):
        self.mainLayout = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.setCentralWidget(self.mainLayout)

    def setMenu(self):
        # Quit menu item
        exitAct = QtWidgets.QAction('&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(QtWidgets.qApp.quit)

        # Browse menu item
        browseAct = QtWidgets.QAction("&Open File", self)
        browseAct.setShortcut('Ctrl+O')
        browseAct.setStatusTip('Open file')
        browseAct.triggered.connect(self.file_open)

        # Save menu item
        saveAct = QtWidgets.QAction("&Save", self)
        saveAct.setShortcut('Ctrl+S')
        saveAct.setStatusTip('Save results to database')
        #self.saveAct.setEnabled(False)
        saveAct.triggered.connect(self.file_save)

        # Setting menu bar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)
        fileMenu.addAction(browseAct)
        fileMenu.addAction(saveAct)

    ###########################
    ###### I/O functions ######
    ###########################
    def file_open(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        dialog = QtWidgets.QFileDialog()
        dialog.setOptions(options)
        filename,_ = dialog.getOpenFileName(self, 'Open File')

        if filename != None and filename != "":
            self.loadDB(filename)

    def file_save(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        dialog = QtWidgets.QFileDialog()
        dialog.setOptions(options)
        filename,_ = dialog.getSaveFileName(self, 'Save File')
        if filename != None and filename != "":
            self.saveDB(filename)

    def initDB(self, filename: str):
        """
        Initializes the connection with the database.
        Creates the database if it does not exist.
        """
        db_conn = getDBConnection(filename)
        createGroupTable(db_conn)
        for tp in self.tps:
            createTPCompilTable(db_conn, tp)
            createTPBenchTable(db_conn, tp)
            createTPReportTable(db_conn, tp)
        return db_conn

    def saveDB(self, filename: str):
        db_conn = self.initDB(filename)
        for button in self.buttonGroup.buttons():
            button.saveDB(db_conn)
        db_conn.commit()
        db_conn.close()

    def loadDB(self, filename: str):
        db_conn = getDBConnection(filename)
        # Reading group names from DB
        c = db_conn.cursor()
        cmd = '''SELECT names FROM groups'''
        c.execute(cmd)
        group = c.fetchone()
        while group != None:
            groupButton = self.newGroupButton(group[0])
            groupButton.loadDB(db_conn)
            group = c.fetchone()

        db_conn.close()
