from src.tp import *
from src.project import Project
import os
from src.utils import *

def copy_lines(input_file, output_file, from_k, to_n):
    for i in range(from_k, to_n):
        input_line = input_file.readline()
        output_file.write(input_line)

def applyDiff(
    input_file_path: str,
    diff_file_path: str,
    output_file_path: str) -> int:

    diff_file = open(diff_file_path, 'r', encoding='iso-8859-1')
    input_file = open(input_file_path, 'r', encoding='iso-8859-1')
    output_file = open(output_file_path, 'w', encoding='iso-8859-1')

    cost = 0
    n_line = 0
    diff_line = diff_file.readline()
    while diff_line:
        line = diff_line.split(' ')
        if line[0] == '+': # Ajout
            k = int(line[1]) # L'ajout se fait après la ligne k
            # On recopie les lignes de n_line à k
            copy_lines(input_file, output_file, n_line, k)
            n_line = k
            # On ajoute la nouvelle ligne
            diff_line = diff_file.readline()
            cost += 10 + len(diff_line) - 1 # -1 pour ne pas compter le saut de ligne
            output_file.write(diff_line)

        elif line[0] == '=': # Substitution
            k = int(line[1]) # la substitution se fait à la ligne k
            # On recopie les lignes de n_line à k-1
            copy_lines(input_file, output_file, n_line, k-1)
            # On skip une ligne du diff (la vieille ligne)
            diff_line = diff_file.readline()
            # On ajoute la nouvelle ligne
            diff_line = diff_file.readline()
            cost += 10 + len(diff_line) - 1 # -1 pour ne pas compter le saut de ligne
            output_file.write(diff_line)
            # On lit une ligne sans la copier
            input_line = input_file.readline()
            n_line = k

        elif line[0] == '-': # Destruction
            cost += 10
            k = int(line[1]) # destruction de la ligne k
            # On recopie les lignes de n_line à k-1
            copy_lines(input_file, output_file, n_line, k-1)
            # On lit la ligne sans la copier
            input_line = input_file.readline()
            # On skip la ligne du diff
            diff_line = diff_file.readline()
            n_line = k

        diff_line = diff_file.readline()

    # Copying the remaining lines:
    input_line = input_file.readline()
    while input_line:
        output_file.write(input_line)
        input_line = input_file.readline()

    diff_file.close()
    input_file.close()
    output_file.close()

    return cost

def applyDiffReverse(
    input_file_path: str,
    diff_file_path: str,
    output_file_path: str) -> int:

    diff_file = open(diff_file_path, 'r', encoding='iso-8859-1')
    input_file = open(input_file_path, 'r', encoding='iso-8859-1')
    output_file = open(output_file_path, 'w', encoding='iso-8859-1')

    cost = 0
    n_line = 0
    diff_line = diff_file.readline()
    while diff_line:
        line = diff_line.split(' ')
        if line[0] == '+': # Ajout
            k = int(line[1]) # L'ajout se fait après la ligne k
            # On recopie les lignes de n_line à k
            copy_lines(input_file, output_file, n_line, k)
            n_line = k
            # On skip la ligne du diff
            diff_line = diff_file.readline()
            # On lit une ligne dans la source sans la copier
            input_line = input_file.readline()
            cost += 10 + len(diff_line) - 1 # -1 pour ne pas compter le saut de ligne

        elif line[0] == '=': # Substitution
            k = int(line[1]) # la substitution se fait à la ligne k
            # On recopie les lignes de n_line à k-1
            copy_lines(input_file, output_file, n_line, k-1)
            # On ajoute la nouvelle ligne
            diff_line = diff_file.readline()
            output_file.write(diff_line)
            # On lit une ligne sans la copier
            input_line = input_file.readline()
            # On skip une ligne du diff
            diff_line = diff_file.readline()
            cost += 10 + len(diff_line) - 1 # -1 pour ne pas compter le saut de ligne
            n_line = k

        elif line[0] == '-': # Destruction
            cost += 10
            k = int(line[1]) # destruction de la ligne k
            # On recopie les lignes de n_line à k-1
            copy_lines(input_file, output_file, n_line, k-1)
            # On copie la ligne
            diff_line = diff_file.readline()
            output_file.write(diff_line)
            n_line = k

        diff_line = diff_file.readline()

    # Copying the remaining lines:
    input_line = input_file.readline()
    while input_line:
        output_file.write(input_line)
        input_line = input_file.readline()

    diff_file.close()
    input_file.close()
    output_file.close()

    return cost

def compareFiles(target_name: str, patched_file_name: str, cost: int):
    target = open(target_name, 'r', encoding='iso-8859-1').readlines()
    patched = open(patched_file_name, 'r', encoding='iso-8859-1').readlines()
    if len(target) != len(patched):
        return Result(Status.ERROR,
            comment = 'result and target have different length!'
        )
    for i in range(len(target)):
        if target[i] != patched[i]:
            return Result(Status.ERROR,
                comment = 'Line %i incorrect:\n%s\nshould be\n%s' % (
                    i,
                    patched[i],
                    target[i]
                )
            )
    return Result(Status.OK)


class Diff(TP):

    def __init__(self):
        super(Diff, self).__init__("Diff")

        # 0.5 point pour la compilation du projet
        self.compil_barem = 0.5

        # Small tests
        self.benchmarks = [
            self.makeBenchmark("Test ajout",
                "tps/diff/benchmarks/test_add/source",
                "tps/diff/benchmarks/test_add/target",
                barem = 0.2,
                opt_cost = 26
            ),
            self.makeBenchmark("Test destruction",
                "tps/diff/benchmarks/test_destruction/source",
                "tps/diff/benchmarks/test_destruction/target",
                barem = 0.2,
                opt_cost = 10
            ),
            self.makeBenchmark("Test substitution",
                "tps/diff/benchmarks/test_subst/source",
                "tps/diff/benchmarks/test_subst/target",
                barem = 0.2,
                opt_cost = 13
            ),
            self.makeBenchmark("Test source vide",
                "tps/diff/benchmarks/test_empty_source/source",
                "tps/diff/benchmarks/test_empty_source/target",
                barem = 0.2,
                opt_cost = 98
            ),
            self.makeBenchmark("Test target vide",
                "tps/diff/benchmarks/test_empty_target/source",
                "tps/diff/benchmarks/test_empty_target/target",
                barem = 0.2,
                opt_cost = 30
            ),
        ]

        # Real benchmarks
        self.benchmarks += [
            self.makeBenchmark("benchmark_00",
                "tps/diff/benchmarks/benchmark_00/source",
                "tps/diff/benchmarks/benchmark_00/target",
                barem = 0.5,
                opt_cost = 10475),
            self.makeBenchmark("benchmark_01",
                "tps/diff/benchmarks/benchmark_01/source",
                "tps/diff/benchmarks/benchmark_01/target",
                barem = 1.0,
                opt_cost = 12699),
            self.makeBenchmark("benchmark_02",
                "tps/diff/benchmarks/benchmark_02/source",
                "tps/diff/benchmarks/benchmark_02/target",
                barem = 1.0,
                opt_cost = 3626),
            self.makeBenchmark("benchmark_03",
                "tps/diff/benchmarks/benchmark_03/source",
                "tps/diff/benchmarks/benchmark_03/target",
                barem = 1.0,
                opt_cost = 19187),
            self.makeBenchmark("benchmark_04",
                "tps/diff/benchmarks/benchmark_04/source",
                "tps/diff/benchmarks/benchmark_04/target",
                barem = 2.0,
                opt_cost = 3748),

        ]

        self.questions = self.questions + [
             Question("Modélisation en PLNE", barem = 1.0),
             Question("Graphe de dépendance", barem = 1.0),
             Question("Programme : clarté", barem = 1.0),
             Question("Programme : efficacité", barem = 1.0),
             Question("Programme : documentation", barem = 1.0),
             Question("Explication du principe du programme", barem = 1.0),
             Question("Analyse coût théorique", barem = 3.0),
             Question("Compte rendu expérimentation", barem = 2.0),
             Question("Si le coût du patch était en octet", barem = 1.0),
             Question("Bonus: programmation PLNE", barem = 1.0),
        ]

        self.exe = "computePatchOpt"

    def makeBenchmark(self, name: str, source: str, target: str, barem = 0.0, opt_cost = 0):
        b = Benchmark(name, barem = barem)
        b.source = source
        b.target = target
        b.opt_cost = opt_cost
        return b

    def getPatchFile(self, project, benchmark):
        return os.path.join(
            self.getTmpFolder(project, benchmark),
            "patch"
        )

    def cmd(self, project, benchmark):
        res = ["time", project.exe, benchmark.source, benchmark.target,
            self.getPatchFile(project, benchmark)]
        print('running %s' % ' '.join(res))
        return res

    def checkResult(self, project, benchmark, result):
        if result == 0:
            patch_file = self.getPatchFile(project, benchmark)
            if not os.path.isfile(patch_file):
                return Result(Status.ERROR,
                    comment = "patch file %s does not exist" % patch_file)

            res_file_name = os.path.join(
                self.getTmpFolder(project, benchmark),
                'result_of_patch'
            )
            cost = applyDiff(benchmark.source,
                patch_file,
                res_file_name)

            result = compareFiles(benchmark.target,res_file_name, cost)
            if result.status != Status.OK:
                return result
            else:
                result.comment = "Coût du patch : %i\nL'application du patch a donné le bon fichier\n" % cost

            cost2 = applyDiffReverse(benchmark.target,
                self.getPatchFile(project, benchmark),
                res_file_name)
            result_rev = compareFiles(benchmark.source,res_file_name, cost2)
            result.merge(result_rev)

            if result.status != Status.OK:
                return result
            else:
                result.comment += "L'application du patch inverse a donné le bon fichier"

            # Setting grade
            if cost > benchmark.opt_cost:
                result.grade = benchmark.barem / 2
                result.merge( Result(
                    comment = "Cost is not optimal",
                    status = Status.BOF
                ))
            else:
                result.grade = benchmark.barem
                result.comment += "\n Cost is optimal"
            return result

        else:
            return Result(Status.ERROR)
