import os
import subprocess
import sqlite3
from threading import Lock

def getProjectFolder() -> str:
    return 'projects'

def createProjectFolder() -> None:
    folder_name = getProjectFolder()
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

def silentRunner(cmd, out = subprocess.PIPE, err = subprocess.PIPE):
    """
    Silently runs an external command.
    Returns the process.

    Arguments
    ---------
    cmd: function
        Function that returns the command to run
    """
    p = subprocess.Popen(cmd(),
        stdout=out,
        stderr=err)
    (result, error) = p.communicate()
    return p

def runner(cmd, out = subprocess.PIPE, err = subprocess.PIPE):
    """
    Silently runs an external command.
    Returns the process.

    Arguments
    ---------
    cmd: function
        Function that returns the command to run
    """
    p = subprocess.Popen(cmd(),
        stdout = out,
        stderr = out)
    return p

# DATABASE Management
def getDBConnection(db_name: str):
    try:
        conn = sqlite3.connect(db_name)
        return conn
    except Error as e:
        print(e)
    return None

def createTable(conn, create_table_sql: str, *args) -> None:
    try:
        c = conn.cursor()
        c.execute(create_table_sql, args)
    except sqlite3.IntegrityError as e:
        print(e)

def createGroupTable(conn) -> None:
    createTable(conn,
        """ CREATE TABLE IF NOT EXISTS groups (
            names text PRIMARY KEY
        ); """
    )

def createTPCompilTable(conn, tp) -> None:
    createTable(conn,
        """ CREATE TABLE IF NOT EXISTS {} (
            id text PRIMARY KEY,
            path text,
            status text,
            comment text
        ); """.format(tp.getCompilTableName())
    )

def createTPBenchTable(conn, tp) -> None:
    createTable(conn,
        """ CREATE TABLE IF NOT EXISTS {} (
            id text NOT NULL,
            bench_name text NOT NULL,
            status text,
            grade REAL,
            comment text,
            PRIMARY KEY (id, bench_name)
        ); """.format(tp.getBenchTableName())
    )

def createTPReportTable(conn, tp) -> None:
    createTable(conn,
        """ CREATE TABLE IF NOT EXISTS {} (
            id text NOT NULL,
            question_name text NOT NULL,
            grade REAL,
            comment text,
            PRIMARY KEY (id, question_name)
        ); """.format(tp.getReportTableName())
    )

from enum import Enum
class Precision(Enum):
    TITLE = 0
    INPUT = 1
    OUTPUT = 1
    NORMAL = 3
    DETAIL = 5
    ERROR = 0

precision_level = 0

def print_trace(message, precision):
    tabs = '\t'.join(['' for i in range(precision.value)])
    print(tabs + message.replace('\n','\n' + tabs))

def logTrace(message, precision):
    if precision.value <= precision_level:
        print_trace(message, precision)

lock = Lock()

def makeLogFile():
    with lock:
        folder_name = 'logs'
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        logs = os.listdir(folder_name)
        if len(logs) == 0:
            log_file = os.path.join(folder_name, 'log0')
        else:
            i = 0
            while True:
                if 'log%i' % i in logs:
                    i += 1
                else:
                    log_file = os.path.join(folder_name, 'log%i' % i)
                    break
        out_file = open(log_file, 'wb')
        in_file = open(log_file, 'rb', 1)
        return (out_file, in_file, log_file)
