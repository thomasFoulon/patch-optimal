import sys
from src.project import Project
from src.tp import *
from src.utils import silentRunner

def run_bench(project, tp, bench):
    print('Running benchmark %s' % bench.name)
    cmd = tp.cmd(project, bench)
    print("command: %s" % cmd)
    p = silentRunner(lambda: cmd)
    res = p.wait()
    result = tp.checkResult(project, bench, res)
    print(result.toString(bench.barem))

def run(tp, path, bench_name = None):
    project = Project(path, tp.exe)
    try:
        print('Compiling project')
        project.compile()
    except:
        print('Compilation unsuccessful')
        sys.exit(2)
    if bench_name != None:
        bench = next(bench for bench in tp.benchmarks if bench.name == bench_name)
        run_bench(project, tp, bench)
    else:
        for bench in tp.benchmarks:
            run_bench(project, tp, bench)
