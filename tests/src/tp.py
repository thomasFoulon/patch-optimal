from abc import ABC, abstractmethod
from enum import IntEnum
import os

class Status(IntEnum):
    OK = 1
    WAITING = 2
    RUNNING = 3
    BOF = 4
    ERROR = 5

    def ofString(s: str):
        s = s.lower()
        if 'ok' in s:
            return Status.OK
        elif 'error' in s:
            return Status.ERROR
        elif 'bof' in s:
            return Status.BOF
        elif 'waiting' in s:
            return Status.WAITING
        elif 'running' in s:
            return Status.RUNNING
        return None

    def toString(status):
        if status == Status.OK:
            return "ok"
        elif status == Status.ERROR:
            return "error"
        elif status == Status.BOF:
            return "bof"
        elif status == Status.WAITING:
            return "waiting"
        elif status == Status.RUNNING:
            return "running"
        return None

    def merge(s1, s2):
        return max(s1,s2)

from src.project import Project

class Result:

    def __init__(self,
        status = Status.WAITING,
        grade = None,
        comment = None):
        self.comment = comment
        self.grade = float(grade) if grade != None else None
        self.status = status

    def getComment(self) -> str:
        return self.comment

    def getGrade(self) -> float:
        return self.grade

    def getStatus(self) -> Status:
        return self.status

    def toString(self, barem: float) -> str:
        return 'Note: %s/%f\nStatus: %s\nComment: %s\n' % (
            str(self.grade) if self.grade != None else 'NA',
            barem,
            Status.toString(self.status),
            self.comment if self.comment != None else ''
        )

    def merge(self, res) -> None:
        if self.comment is None:
            self.comment == res.comment
        elif res.comment is not None:
            self.comment += '\n' + res.comment
        self.status = Status.merge(self.status, res.status)

class Benchmark:

    def __init__(self, name: str, barem = 0.0):
        #benchmark name
        self.name = name
        self.barem = barem

class Question:

    def __init__(self, name: str, barem = 0.0):
        self.name = name
        self.barem = barem

class TP(ABC):

    # barem of the compilation process
    compil_barem = 2.0

    def __init__(self, name: str):
        """
        Parameters
        ----------
        name : str
            Name of the TP
        exe: str
            Name of the executable file that the project must provide
        """

        self.name = name
        self.benchmarks = []
        self.questions = [
            Question('Clarté du rapport', 1.0),
        ]
        self.exe = None

    # @abstractmethod
    # def check(self, benchmark, project: Project) -> Result:
    #     pass

    def getBenchTableName(self):
        return '%s_benchs' % self.name.replace(' ', '')

    def getReportTableName(self):
        return '%s_report' % self.name.replace(' ', '')

    def getCompilTableName(self):
        return '%s_compil' % self.name.replace(' ', '')

    def getTmpFolder(self, project, bench):
        folder_path = os.path.join('_tmp',
            os.path.basename(project.getFolder()),
            self.name,
            bench.name
        ).replace(' ','_')
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        return folder_path

    @abstractmethod
    def cmd(self, project, benchmark):
        pass
