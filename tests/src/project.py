import os
from src.utils import *

class ProjectException(Exception):
    def __init__(self, message):
        super().__init__(message)

class Project:
    """
    This class handles paths and compilation of a student project.

    Attributes
    ----------
    folder: string
        The path of the project folder
    Makefile: string
        The path of the Makefile
        It must be called 'Makefile' and located at the root of the project folder
    bin_folder: string
        The path of the bin folder
        It must be called 'bin' and located at the root of the project folder.
    exe: string
        The path of the executable
        It must be located within the bin folder.
    """

    def __init__(self, folder: str = None, exe: str = None):
        """
        Project constructor

        Arguments
        ---------
        folder: str
            Name of the project folder
        exe: str
            Name of the generated executable
        """
        if folder:
            self.setFolder(folder)
            if exe:
                self.setExe(exe)

    def getFolder(self) -> str:
        """
        It well defined, returns the absolute folder of the project.
        Otherwise, returns None.
        """
        if hasattr(self, 'folder'):
            return self.folder
        return None

    def load(self):
        """
        Checks that provided paths are well defined, then tries to compile the project.

        ------
        ProjectException if any of the following happens:
            the project folder is not found
            the Makefile does not exist
            there is no bin folder
            the exe does not exist in the bin folder
        """
        if not hasattr(self, 'folder'):
            raise ProjectException('Source folder has not been specified')

        logTrace('Opening project %s' % self.folder, Precision.TITLE)

        if not self.srcFolderExists():
            raise ProjectException('Folder %s not found' % self.folder)

        if not self.makefileExists():
            raise ProjectException('No Makefile found in folder %s' % self.folder)

        self.compile()

        if not self.binFolderExists():
            raise ProjectException('Folder %s not found' % self.bin_folder)

        if not self.exeExists():
            raise ProjectException('No executable %s found in folder %s' % (self.exe, self.bin_folder))

    def setFolder(self, folder: str) -> None:
        self.folder = folder
        self.Makefile = os.path.join(self.folder, 'Makefile')
        self.bin_folder = os.path.join(self.folder, 'bin')

    def setExe(self, exe: str) -> None:
        if self.folder:
            self.exe = os.path.join(self.bin_folder, exe)

    def srcFolderExists(self) -> bool:
        """
        Checks if the provided folder exists

        Returns
        -------
        bool
            true if the folder exists
        """
        return os.path.exists(self.folder)

    def makefileExists(self) -> bool:
        """
        Checks if the provided folder contains a Makefile

        Returns
        -------
        bool
            true if the Makefile exists
        """
        return os.path.isfile(self.Makefile)

    def binFolderExists(self) -> bool:
        """
        Checks if the project folder contains a bin folder

        Returns
        -------
        bool
            true if the bin folder exists
        """
        return os.path.exists(self.bin_folder)

    def exeExists(self) -> bool:
        """
        Checks if the bin folder contains the executable

        Returns
        -------
        bool
            true if the executable exists
        """
        return os.path.isfile(self.exe)

    def compile(self) -> None:
        """
        Compiles the project

        Raises
        ------
        ProjectException
            if compilation fails
        """

        logTrace('Compiling project', Precision.NORMAL)

        compile_cmd = [
            'make',
            '-C',
            self.folder,
            '-f',
            'Makefile',
        ]
        p = silentRunner(lambda: compile_cmd)
        rc = p.wait()
        if rc == 0:
            logTrace('Compilation sucessfull', Precision.NORMAL)
        else:
            raise ProjectException('Compilation failed')
