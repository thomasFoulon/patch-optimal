/**
 * \file patch.c
 * \authors {Julien BITAILLOU, Thomas FOULON}
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <patch.h>

#define NB_OTHERS 4

/**
 * \fn instruction *new_instruction(instruction_type type, uint32_t affected_line, char *line_A, char *line_B)
 * \brief Allocation d'une nouvelle instruction de patch.
 * 
 * \param type type d'instruction (addition, substitution ou destruction)
 * \param affected_line la ligne du flot d'entrée affectée par l'instruction
 * \param line_A la ligne du fichier d'entrée (ne peut être NULL si destruction ou substitution
 * \param line_B la ligne du fichier de sortie (ne peut être NULL si addition ou substitution)
 *
 * \return la nouvelle instruction créée.
 */
instruction *new_instruction(instruction_type type, uint32_t affected_line, char *line_A, char *line_B){
    if(type == SUBSTITUTION || type == ADDITION) assert(line_B != NULL);
    if(type == SUBSTITUTION || type == DESTRUCTION) assert(line_A != NULL);
    instruction *i = malloc(sizeof(instruction));
    i->type = type;
    i->affected_line = affected_line;
    i->line_A = line_A;
    i->line_B = line_B;
    i->previous = NULL;
    return i;
}

/**
 * \fn delete_instruction(instruction *i)
 * \brief Destruction d'une instruction de patch.
 * 
 * \param i l'instruction à détruire
 */
void delete_instruction(instruction *i){
    assert(i != NULL);
    free(i);
}

/**
 * \fn *new_patch(uint32_t nb_lines_A, uint32_t nb_lines_B)
 * \brief Allocation d'un nouveau patch vide (sans instruction).
 * 
 * \param nb_lines_A le nombre de ligne du fichier d'entrée
 * \param nb_lines_B le nombre de lignes du fichier de sortie
 *
 * \return le nouveau patch vide.
 */
patch *new_patch(uint32_t nb_lines_A, uint32_t nb_lines_B){
    patch *p = malloc(sizeof(patch));
    p->nb_lines_A = nb_lines_A;
    p->nb_lines_B = nb_lines_B;
    p->cost = 0;
    p->last = NULL;
    return p;
}

/**
 * \fn delete_patch(patch *p, BOOL delete_instructions)
 * \brief Destruction d'un patch et potentiellement de ses instructions.
 * 
 * \param p le patch à détruire
 * \param delete_instructions les instructions doivent elles être détruites également ?
 */
void delete_patch(patch *p, BOOL delete_instructions){
    assert(p != NULL);
    if(delete_instructions){
        instruction *current = p->last, *next;
        while(current != NULL){
            // Deleting instructions of the patch
            next = current->previous;
            delete_instruction(current);
            current = next;
        }
    }
    free(p);
}

/**
 * \fn add_instruction(patch *p, instruction *i)
 * \brief Ajout d'une instruction à un patch.
 * 
 * \param p le patch auquel ajouter l'instruction
 * \param i l'instruction à ajouter
 * \param cost le coût de l'instruction à ajouter au patch
 */
void add_instruction(patch *p, instruction *i, uint32_t cost){
    assert(p != NULL);
    if(p->last == NULL){
        // First instruction
        p->last = i;
    }
    else{
        i->previous = p->last;
        p->last = i;
    }
    p->cost += cost;
}

uint32_t to_string_rec(instruction *i, char **str_patch){
    uint32_t number, nb_digits, size_line_A, size_line_B, sizeof_inst, sizeof_patch;
    char *str_inst = NULL, type;
    // Determining the size of the instruction in terms of characters
    number = i->affected_line;
    if(number == 0) nb_digits = 1;
    else{
        nb_digits = 0;
        while(number != 0){ ++nb_digits; number /= 10; }
    }
    size_line_A = i->line_A!=NULL?strlen(i->line_A):0;
    size_line_B = i->line_B!=NULL?strlen(i->line_B):0;
    sizeof_inst = nb_digits + size_line_A + size_line_B + NB_OTHERS;
    str_inst = malloc(sizeof(char)*sizeof_inst);

    // Creating the instruction
    switch(i->type){
        case ADDITION:
            type = '+';
            sprintf(str_inst, "%c %u\n%s", type, i->affected_line, i->line_B);
            break;
        case SUBSTITUTION:
            type = '=';
            sprintf(str_inst, "%c %u\n%s%s", type, i->affected_line, i->line_A, i->line_B);
            break;
        case DESTRUCTION:
            type = '-';
            sprintf(str_inst, "%c %u\n%s", type, i->affected_line, i->line_A);
            break;
        default:
            exit(-1);
    }
    if(i->previous == NULL){
        *str_patch = str_inst;
        return sizeof_inst;
    }
    else{
        sizeof_patch = to_string_rec(i->previous, str_patch);
        sizeof_patch += sizeof_inst - 1; // minus one of the two '\0'
        *str_patch = realloc(*str_patch, sizeof_patch);
        strcat(*str_patch, str_inst);
        free(str_inst);
        return sizeof_patch;
    }
}

/**
 * \fn *to_string(patch *p)
 * \brief Traduction d'un patch sous forme de chaîne de caractères.
 * 
 * \param p le patch à traduire
 * 
 * \return la traduction sous forme de chaîne de caractère du patch.
 */
char *to_string(patch *p){
    char *str_patch;
    to_string_rec(p->last, &str_patch);
    return str_patch;
}