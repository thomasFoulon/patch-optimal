/**
 * \file computePatchOpt.c
 * \authors {Julien BITAILLOU, Thomas FOULON}
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

#include <patch.h>

#define NB_ARGS 4

char *begin_patch_optimal(char *file1, char *file2);

char *patch_optimal(char **lines1, uint32_t nb_lines1, char **lines2, uint32_t nb_lines2);

void f(uint32_t i, uint32_t j, char **lines1, char **lines2, patch **patches, patch **previous, uint32_t nb_lines1);

uint32_t c(char *line1, char *line2);

uint32_t nb_lines(char *file){
    if(file == NULL) return 0;
    uint32_t i = 0, nb_lines = 0;
    while(file[i] != '\0'){
        ++i;
        if(file[i] == '\n') ++nb_lines;
    }
    return nb_lines;
}

void set_size_of_lines(char *file, char **lines){
    uint32_t i = 0, j = 0, line_size = 0;
    // Computing line sizes
    while(file[i] != '\0'){
        ++line_size;
        if(file[i] == '\n'){
            lines[j] = malloc(sizeof(char)*(line_size+1));
            line_size = 0;
            ++j;
        }
        ++i;
    }
}

void fill_lines(char *file, char **lines){
    uint32_t i = 0, j = 0, k = 0;
    while(file[i] != '\0'){
        lines[j][k] = file[i];
        ++k;
        if(file[i] == '\n'){
            lines[j][k] = '\0';
            ++j;
            k = 0;
        }
        ++i;
    }
}

void free_lines(char **lines, uint32_t nb_lines){
    for(uint32_t i = 0; i < nb_lines; ++i){
        free(lines[i]);
    }
    free(lines);
}

char *begin_patch_optimal(char *file1, char *file2){
    uint32_t nb_lines1, nb_lines2;
    char **lines1 = NULL, **lines2 = NULL, *patch;
    // Parsing of the first file
    nb_lines1 = nb_lines(file1);
    if(nb_lines1 != 0){
        lines1 = malloc(nb_lines1*sizeof(char *));
        set_size_of_lines(file1, lines1);
        fill_lines(file1, lines1);
    }
    // Parsing of the second file
    nb_lines2 = nb_lines(file2);
    if(nb_lines2 != 0){
        lines2 = malloc(nb_lines2*sizeof(char *));
        set_size_of_lines(file2, lines2);
        fill_lines(file2, lines2);
    }
    patch = patch_optimal(lines1, nb_lines1, lines2, nb_lines2);
    free_lines(lines1, nb_lines1);
    free_lines(lines2, nb_lines2);
    return patch;
}

char *patch_optimal(char **lines1, uint32_t nb_lines1, char **lines2, uint32_t nb_lines2){
    // One patch by column (see the graph)
    patch *patches[nb_lines1+1], *previous;
    char *patch_opt;
    for(uint32_t j = 0; j <= nb_lines2; ++j){
        for(uint32_t i = 0; i <= nb_lines1; ++i){
            f(i, j, lines1, lines2, patches, &previous, nb_lines1);
        }
    }
    patch_opt = to_string(patches[nb_lines1]); // The patch of f(nb_lines1, nb_lines2) !
    for(uint32_t i = 0; i <= nb_lines1; ++i){
        delete_patch(patches[i], FALSE);
    }
    return patch_opt;
}

void f(uint32_t i, uint32_t j, char **lines1, char **lines2, patch **patches, patch **previous, uint32_t nb_lines1){
    patch *p;
    instruction *inst;
    uint32_t cost1, cost2, cost3; // same order as Bellman equation
    p = new_patch(i, j);
    if(j == 0){
        // First line (no dependence)
        if(i == 0){
            p->cost = 0;
        }
        else{
            // This patch is a sequel of the previous one
            p->last = patches[i-1]->last;
            p->cost = patches[i-1]->cost;
            inst = new_instruction(DESTRUCTION, i, lines1[i-1], NULL);
            add_instruction(p, inst, 10);
        }
        patches[i] = p;
    }
    else{
        // Dependence of the previous line (j - 1) on the array patches
        if(i == 0){
            // No dependence for this case (first column)
            p = new_patch(i, j);
            p->last = patches[0]->last;
            p->cost = patches[0]->cost;
            inst = new_instruction(ADDITION, lines1==NULL?0:j-1, NULL, lines2[j-1]);
            add_instruction(p, inst, 10 + strlen(lines2[j-1]));
            *previous = p;
        }
        else{
            // We have to determine the patch with the best cost (Bellman equation)
            cost1 = patches[i]->cost + 10 + strlen(lines2[j-1]);
            cost2 = patches[i-1]->cost + c(lines1[i-1], lines2[j-1]);
            cost3 = (*previous)->cost + 10;
            if(cost1 < cost2 && cost1 < cost3){
                // The patch (i, j - 1) + addition is the best (case (1.1) of Bellman equation)
                p->last = patches[i]->last;
                p->cost = patches[i]->cost;
                inst = new_instruction(ADDITION, i, NULL, lines2[j-1]);
                add_instruction(p, inst, cost1 - patches[i]->cost);
            }
            else if(cost2 < cost1 && cost2 < cost3){
                // The patch (i - 1, j - 1) + substitution is the best (case (1.2) of Bellman equation)
                p->last = patches[i-1]->last;
                p->cost = patches[i-1]->cost;
                if(cost2 == patches[i-1]->cost){
                    // Lines are equals (no more instruction)
                }
                else{
                    inst = new_instruction(SUBSTITUTION, i, lines1[i-1], lines2[j-1]);
                    add_instruction(p, inst, cost2 - patches[i-1]->cost);
                }
            }
            else{
                // The previous patch (i - 1, j) + destruction is the best (case (1.3) of Bellman equation)
                p->last = (*previous)->last;
                p->cost = (*previous)->cost;
                inst = new_instruction(DESTRUCTION, i, lines1[i-1], NULL);
                add_instruction(p, inst, 10);
            }
            delete_patch(patches[i-1], FALSE);
            patches[i-1] = *previous;
            *previous = p;
        }
        if(i == nb_lines1){
            // We are at (nb_lines1, j)
            delete_patch(patches[i], FALSE);
            patches[i] = p;
        }
    }
}

uint32_t c(char *line1, char *line2){
    if(strcmp(line1, line2) == 0) return 0;
    else return 10 + strlen(line2);
}

/**
 * \fn int main(int argc, char *argv[])
 * \brief Entrée du programme.
 * 
 * \param argc nombre d'arguments du programme
 * \param argv paramètres du programme
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */
int main(int argc, char *argv[]) {
    int fd_file1, fd_file2;
    FILE *fp_patch;
    long size_file1, size_file2;
    char *file1 = NULL, *file2 = NULL, *patch;
    
    if(argc != NB_ARGS){
        printf("Utilisation : %s file1 file2 patch_out\n", argv[0]);
        exit(1);
    }
    
    // Ouverture des fichiers
    fd_file1 = open(argv[1], O_RDONLY);
    fd_file2 = open(argv[2], O_RDONLY);
    
    // Récupération des tailles des deux fichiers
    size_file1 = lseek(fd_file1, 0L, SEEK_END);
    size_file2 = lseek(fd_file2, 0L, SEEK_END);
    lseek(fd_file1, 0L, SEEK_SET);
    lseek(fd_file2, 0L, SEEK_SET);
    
    // Mapping des fichiers dans la mémoire
    if(size_file1 > 0) file1 = mmap(NULL, size_file1, PROT_READ, MAP_PRIVATE, fd_file1, 0);
    if(size_file2 > 0) file2 = mmap(NULL, size_file2, PROT_READ, MAP_PRIVATE, fd_file2, 0);
    
    // Fermeture des fichiers
    close(fd_file1);
    close(fd_file2);
    
    // Début de l'algorithme de recherche de patch optimal
    patch = begin_patch_optimal(file1, file2);
    
    // Enregistrement du patch dans le fichier
    fp_patch = fopen(argv[3], "w");
    if(patch != NULL){
        fwrite(patch, sizeof(char), strlen(patch), fp_patch);
        free(patch);
    }
    fclose(fp_patch);
    
    return 0;
}
